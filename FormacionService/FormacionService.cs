﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace FormacionService
{
    class FormacionService : ServiceBase
    {
        FormacionServiceManager m_formacionManager;

        public FormacionService()
        {
            this.ServiceName = "FormacionService";
            this.CanStop = true;
            this.CanPauseAndContinue = false;
            this.AutoLog = true;

            m_formacionManager = new FormacionServiceManager();

        }

        protected override void OnStart(string[] args)
        {
            this.StartService();
        }

        protected override void OnStop()
        {
            this.StopService();
        }

        public void StartService()
        {
            m_formacionManager.Start();
        }

        public void StopService()
        {
            m_formacionManager.Stop();
        }
    }
}
