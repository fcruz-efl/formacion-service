﻿using DllWcfUtility;
using log4net;
using ServiceFormacion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace FormacionService
{
    class FormacionServiceManager
    {
        private static ILog log = LogManager.GetLogger("FileLog");
        private static ILog logError = LogManager.GetLogger("FileLogError");

        private bool m_running;
        private bool m_stopped;
        private Thread m_thread;
        private WcfFormacionClient m_formacionClient;
        private DateTime m_getSessionsTime;

        public FormacionServiceManager()
        {
            m_running = false;
            m_stopped = false;
            m_thread = new Thread(new ThreadStart(Run));
            m_formacionClient = new WcfFormacionClient();
        }

        public void Start()
        {
            
            log.Info("FormacionServiceManager.Start Start");
            m_stopped = false;
            m_thread.Start();
            m_running = m_thread.IsAlive;
            log.Info("FormacionServiceManager.Start End");
        }

        public void Stop()
        {
            
            log.Info("FormacionServiceManager.Stop Start");
            log.Info("FormacionServiceManager.Stop Set m_running to false");
            m_running = false;
            m_stopped = true;
            log.Info("FormacionServiceManager.Stop End");
        }

        public void Run()
        {
            log.Info("FormacionServiceManager.Run Start");
            

            while (m_running && !m_stopped)
            {
                BeanList<Event> sessions;
                try
                {
                    log.Info("FormacionServiceManager.Run Nueva iteración");
                    if (m_formacionClient == null)
                    {
                        m_formacionClient = new WcfFormacionClient();
                    }

                    //UpdatePastSessions();
                    
                    UpdateSessions();
                    UpdateSessionsAssist();

                    sessions = m_formacionClient.GetSessionsReminder();
                    SendReminderMails(sessions);

                    DateTime now = DateTime.Now;
                    if (now.Hour == 12)
                    {
                        sessions = m_formacionClient.GetGreetingSessions();
                        SendGreetingsMails(sessions);

                        sessions = m_formacionClient.GetAbsenteeSessions();
                        SendAbsenteesMails(sessions);
                    }
                    
                    log.Info("FormacionServiceManager.Run Sleep");
                    Thread.Sleep(300000);
                    log.Info("FormacionServiceManager.Run Fin iteración");
                    
                }
                catch (Exception err) 
                {
                    logError.Error("FormacionServiceManager.Run Error: " + err.Message);
                }
               
            }
            log.Info("FormacionServiceManager.Run End Terminan los hilos m_stopped: "+ m_stopped + " m_running: " + m_running);
            if (m_stopped)
            {
                log.Info("FormacionServiceManager.Run End Terminan los hilos m_stopped: " + m_stopped + " m_running: " + m_running);
                m_thread.Abort();
            }
            else
            {
                log.Info("FormacionServiceManager.Run End Terminan los hilos m_stopped: " + m_stopped + " m_running: " + m_running + ". Reiniciamos hilos");
                this.Start();
            }
            //m_thread.Join();   
        }

        private void UpdateSessions()
        {
            log.Info("FormacionServiceManager.UpdateSessions Start");
            BeanList<Event> sessions;
            if (m_formacionClient == null)
            {
                m_formacionClient = new WcfFormacionClient();
            }
            if (m_getSessionsTime == DateTime.MinValue)
            {
                m_getSessionsTime = DateTime.Now;
            }
            DateTime now = DateTime.Now;
            TimeSpan diff = (now - m_getSessionsTime);
            double diffTime = diff.TotalMinutes;
            if (diffTime >= 0)
            {
                log.Info("FormacionServiceManager.UpdateSessions Updating sessions");
                BeanList<Program> programs = m_formacionClient.GetPrograms(1);
                if (programs != null && programs.beanList != null)
                {
                    foreach (Program program in programs.beanList)
                    {
                        if (program.Id != 28)
                        {
                            try
                            {
                                sessions = m_formacionClient.GetProgramEvents(program.WebExId, 1);
                                if (sessions != null && sessions.beanList != null)
                                {

                                    log.Info("FormacionServiceManager.UpdateSessions Updating session attendess... ");
                                    foreach (Event session in sessions.beanList)
                                    {
                                        //Console.WriteLine("Actualizando sesion " + session.Name + " (" + session.StartDate + ") del programa " + program.Name);
                                        log.Info("Actualizando sesion " + session.Name + " (" + session.StartDate + ") del programa " + program.Name);
                                        try
                                        {
                                            m_formacionClient.UpdateSessionAttendees(session.WebExId);
                                        }
                                        catch (Exception err)
                                        {
                                            logError.Error("FormacionServiceManager.UpdateSessions Error updating session " + session.WebExId + ": " + err.Message);
                                        }
                                    }

                                }
                            }
                            catch (Exception err)
                            {
                                logError.Error("FormacionServiceManager.UpdateSessions Error updating program " + program.WebExId + ": " + err.Message);
                            }
                        }

                    }
                }
                m_getSessionsTime = DateTime.Now;
            }

            log.Info("FormacionServiceManager.UpdateSessions End");
        }

        private void UpdatePastSessions()
        {
            log.Info("FormacionServiceManager.UpdatePastSessions Start");
            BeanList<Event> sessions;
            if (m_formacionClient == null)
            {
                m_formacionClient = new WcfFormacionClient();
            }
            if (m_getSessionsTime == DateTime.MinValue)
            {
                m_getSessionsTime = DateTime.Now;
            }
            DateTime now = DateTime.Now;
            TimeSpan diff = (now - m_getSessionsTime);
            double diffTime = diff.TotalMinutes;

            log.Info("FormacionServiceManager.UpdatePastSessions Updating sessions");
            BeanList<Program> programs = m_formacionClient.GetPrograms(1);
            if (programs != null && programs.beanList != null)
            {
                foreach (Program program in programs.beanList)
                {
                    if (program.Id != 28)
                    {
                        sessions = m_formacionClient.GetPastProgramEvents(program.WebExId, 1);
                        if (program.Name.Equals("Relaciones Institucionales", StringComparison.CurrentCultureIgnoreCase))
                        {
                            string ese = "";
                            ese += "eee";
                        }
                        if (sessions != null && sessions.beanList != null)
                        {
                            log.Info("FormacionServiceManager.UpdatePastSessions Updating session attendess... ");
                            foreach (Event session in sessions.beanList)
                            {
                                m_formacionClient.UpdateSessionAttendees(session.WebExId);
                            }
                        }
                    }
                }


            }

            log.Info("FormacionServiceManager.UpdatePastSessions End");
        }

        private void UpdateSessionsAssist()
        {
            log.Info("FormacionServiceManager.UpdateSessionsAssist Start");

            try
            {

                BeanList<Event> sessions;
                if (m_formacionClient == null)
                {
                    m_formacionClient = new WcfFormacionClient();
                }


                log.Info("FormacionServiceManager.UpdateSessionsAssist Updating sessions");
                BeanList<Program> programs = m_formacionClient.GetPrograms(1);
                if (programs != null && programs.beanList != null)
                {
                    foreach (Program program in programs.beanList)
                    {
                        try
                        {
                            sessions = m_formacionClient.GetUpdateAssistPastProgramEvents(program.WebExId, 1);
                            if (sessions != null && sessions.beanList != null)
                            {
                                log.Info("FormacionServiceManager.UpdateSessionsAssist Updating session attendess... ");
                                foreach (Event session in sessions.beanList)
                                {
                                    if (program.Id != 28)
                                    {
                                        log.Info("FormacionServiceManager.UpdateSessionsAssist Updating session attendess for session: " + session.WebExId + " Name: " + session.Name + " Family: " + session.Family + " Date: " + session.StartDate);
                                        //Console.WriteLine("Sesion: " + session.WebExId + " Nombre: " + session.Name + " Familia: " + session.Family + " Fecha: " + session.StartDate);
                                        try
                                        {
                                            m_formacionClient.UpdateAttendeesSessionAssist(session.WebExId, 1);
                                        }
                                        catch (Exception err)
                                        {
                                            logError.Error("FormacionServiceManager.UpdateSessionsAssist Error actualizando asistentes de sesion: " + session.WebExId);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception err)
                        {
                            logError.Error("FormacionServiceManager.UpdateSessionsAssist Error recuperando sesiones del programa: " + program.WebExId + ": " + err.Message);
                        }
                    }
                }
            }
            catch (Exception err)
            {
                logError.Error("FormacionServiceManager.UpdateSessionsAssist Error en el servicio: " + err.Message);
            }
            
            log.Info("FormacionServiceManager.UpdateSessionsAssist End");
        }


        private void SendReminderMails(BeanList<Event> ai_sessions)
        {
            log.Info("FormacionServiceManager.SendReminderMails Start");
            if (ai_sessions != null && ai_sessions.beanList != null)
            { 
                foreach(Event session in ai_sessions.beanList)
                {
                    if (session.ReminderEmail)
                    {
                        SendReminderEmail(session);
                    }
                }
            }
            else
            {
                log.Info("FormacionServiceManager.SendReminderMails Error: No hay sesiones pendientes de envío de recordatorios.");
            }
            log.Info("FormacionServiceManager.SendReminderMails End");
        }

        private bool SendReminderEmail(Event ai_session)
        {
            log.Info("FormacionServiceManager.SendReminderEmail Start");
            bool sended;
            DateTime startTime;
            int reminderTime;
            string html;
            html = string.Empty;
            sended = false;
            try
            {
                log.Info("FormacionServiceManager.SendReminderEmail Session: " + ai_session.Id + " WebExId: " + ai_session.WebExId);
                if (ai_session != null)
                {
                    DateTime now = DateTime.Now;
                    startTime = DateTime.Parse(ai_session.StartDate);
                    
                    reminderTime = ai_session.ReminderEmailTime;

                    TimeSpan diff = (startTime - now);
                    double minutes = diff.TotalMinutes;
                    if (!ai_session.ReminderEmailSended)
                    {
                        if (minutes <= reminderTime)
                        {
                            m_formacionClient.SendReminderMails(ai_session.WebExId, 1, reminderTime, 1);
                        }
                    }
                    else
                    {
                        log.Info("FormacionServiceManager.SendReminderEmail Ya se ha enviado el primer recordatorio para esta sesión: " + ai_session.WebExId + ". Enviar segundo recordatorio...");
                        reminderTime = ai_session.ReminderEmailTime2;
                        if (!ai_session.ReminderEmailSended2)
                        {
                            if (minutes <= reminderTime)
                            {
                                m_formacionClient.SendReminderMails(ai_session.WebExId, 2, reminderTime, 1);
                            }
                        }
                        else
                        {
                            log.Info("FormacionServiceManager.SendReminderEmail Ya se han enviado los recordatorios para esta sesión: " + ai_session.WebExId);
                        }
                    }
                }
                else
                {
                    logError.Error("FormacionServiceManager.SendReminderEmail Error: sesión nula");
                }
            }
            catch (Exception err)
            {
                logError.Error("FormacionServiceManager.SendReminderEmail Error: " + err.Message);
                sended = false;
            }
            log.Info("FormacionServiceManager.SendReminderEmail End");
            return sended;
        }

        private void SendGreetingsMails(BeanList<Event> ai_sessions)
        {
            log.Info("FormacionServiceManager.SendGreetingsMails Start");
            if (ai_sessions != null && ai_sessions.beanList != null)
            {
                foreach (Event session in ai_sessions.beanList)
                {
                    if (session.GreetingEmail)
                    {
                        m_formacionClient.SendGreetingMails(session.WebExId, 1);
                    }
                }
            }
            else
            {
                log.Info("FormacionServiceManager.SendGreetingsMails Error: No hay sesiones pendientes de envío de agradecimientos.");
            }
            log.Info("FormacionServiceManager.SendGreetingsMails End");
        }

        private void SendAbsenteesMails(BeanList<Event> ai_sessions)
        {
            log.Info("FormacionServiceManager.SendAbsenteesMails Start");
            if (ai_sessions != null && ai_sessions.beanList != null)
            {
                foreach (Event session in ai_sessions.beanList)
                {
                    if (session.AbsenteeEmail)
                    {
                        m_formacionClient.SendAbsenteeMails(session.WebExId, 1);
                    }
                }
            }
            else
            {
                log.Info("FormacionServiceManager.SendAbsenteesMails Error: No hay sesiones pendientes de envío de agradecimientos.");
            }
            log.Info("FormacionServiceManager.SendAbsenteesMails End");
        }

    }
}
