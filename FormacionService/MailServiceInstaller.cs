﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace FormacionService
{

    [RunInstaller(true)]
    public class MailServiceInstaller : Installer
    {
        private ServiceProcessInstaller processInstaller;
        private ServiceInstaller serviceInstaller;

        public MailServiceInstaller()
        {
            processInstaller = new ServiceProcessInstaller();
            serviceInstaller = new ServiceInstaller();

            processInstaller.Account = ServiceAccount.LocalSystem;
            //serviceInstaller.StartType = ServiceStartMode.Manual;
            serviceInstaller.ServiceName = "LED-FormacionService"; //must match CronService.ServiceName
            processInstaller.Account = System.ServiceProcess.ServiceAccount.User;
            //processInstaller.Password = "ABCabc16";
            //processInstaller.Username = "ext-fcruz";

            Installers.Add(serviceInstaller);
            Installers.Add(processInstaller);
        }
    }
}