﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormacionService
{
    class ServiceMain
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            FormacionService service = new FormacionService();
            if (args.Length == 0)
            {
                System.ServiceProcess.ServiceBase.Run(service);
            }
            else
            {
                if (args[0].Equals("-exe", StringComparison.CurrentCultureIgnoreCase))
                {
                    Console.WriteLine("Console execution...");
                    service.StartService();
                    Console.ReadLine();
                    service.StopService();
                    Console.ReadLine();
                }
            }
        }
    }
}
